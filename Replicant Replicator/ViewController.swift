//
//  ViewController.swift
//  Replicant Replicator
//
//  Created by Chris Garver on 8/29/15.
//  Copyright © 2015 Bottle Rocket Studios. All rights reserved.
//

import UIKit

var replicatorLayers = [CALayer]()

class ViewController: UIViewController {
    
    // MARK:- View Lifecycle
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        removeReplicators()
        jumpingBlocks()
        wavyBars()
    }
    
    // MARK:- UIViewController
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    // MARK:- Helper Methods
    
    func removeReplicators() {
        for layer in replicatorLayers {
            layer.removeAllAnimations()
            layer.removeFromSuperlayer()
        }
        replicatorLayers.removeAll()
    }
    
    // MARK:- Replicator Methods
    
    func wavyBars() {
        
        // configure bar layer
        let bar = CALayer()
        bar.bounds = CGRect(x: 0.0, y: 0.0, width: 16.0, height: 200.0)
        bar.position = CGPoint (x: 8.0, y: 140.0)
        bar.backgroundColor = UIColor.whiteColor().CGColor
        bar.opacity = 0.8
        
        // configure spinning animation for bar
        let rotationAngle: CGFloat = 90 * CGFloat(M_PI / 180)
        let rotationTransform = CATransform3DRotate(bar.transform, rotationAngle, 0.0, 0.0, 1.0)
        
        let spinning = CABasicAnimation(keyPath: "transform")
        spinning.fromValue = NSValue(CATransform3D: bar.transform)
        spinning.toValue = NSValue(CATransform3D: rotationTransform)
        spinning.duration = 1.0
        spinning.autoreverses = true
        spinning.repeatCount = Float.infinity
        
        // add spining animation to bar
        bar.addAnimation(spinning, forKey: nil)
        
        // configure replicator and add bar layer
        let replicator = CAReplicatorLayer()
        replicator.addSublayer(bar)
        
        replicator.bounds = view.bounds
        replicator.position = view.center
        
        replicator.instanceCount = Int(Double(view.frame.width / 16) + 2) // +2 to "fill out" slide animation
        replicator.instanceTransform = CATransform3DMakeTranslation(18.0, 0.0, 0.0)
        replicator.instanceDelay = 0.1
        
        replicator.instanceColor = UIColor.darkBlueishColor().CGColor
        replicator.instanceRedOffset = 1 / Float(replicator.instanceCount)
        
        // configure slideRight animation for replicator
        let slideRight = CABasicAnimation(keyPath: "position.x")
        slideRight.toValue = replicator.position.x + 40
        slideRight.duration = 1.0
        slideRight.autoreverses = true
        slideRight.repeatCount = Float.infinity
        
        // add slideRight animation to replicator
        replicator.addAnimation(slideRight, forKey: nil)
        
        // configure metaReplicator and add replicator to it's sublayers
        let metaReplicator = CAReplicatorLayer()
        metaReplicator.addSublayer(replicator)
        
        metaReplicator.bounds = CGRectOffset(view.bounds, 25.0, 100.0)
        metaReplicator.position = view.center
        
        metaReplicator.instanceCount = Int(view.bounds.height / 100) + 1
        metaReplicator.instanceTransform = CATransform3DMakeTranslation(0.0, 110.0, 10.0)
        metaReplicator.instanceDelay = 1.0
        
        metaReplicator.instanceAlphaOffset = -(1 / Float(metaReplicator.instanceCount))
        
        // append metaReplicator to replicator layers and add to view.layers
        replicatorLayers.append(metaReplicator)
        view.layer.addSublayer(metaReplicator)
    }
    
    func jumpingBlocks() {
        
        let rWidth = CGRectGetWidth(view.bounds)
        let sideLength = max(view.bounds.height, view.bounds.width) / 20
        
        // configure square layer
        let square = CALayer()
        square.bounds = CGRect(x: 0.0, y: 0.0, width: sideLength, height: sideLength)
        square.position = CGPoint(x: sideLength / 2, y: sideLength / 2)
        square.backgroundColor = UIColor.whiteColor().CGColor
        square.opacity = 0.9
        
        // configure tiptoe animation
        let tiptoe = CABasicAnimation(keyPath: "position.y")
        tiptoe.toValue = sideLength * 0.25
        tiptoe.duration = 0.5
        tiptoe.autoreverses = true
        tiptoe.repeatCount = Float.infinity
        
        // add tiptoe animation to square layer
        square.addAnimation(tiptoe, forKey: nil)
        
        // configure slide animation
        let slide = CABasicAnimation(keyPath: "position.x")
        slide.toValue = square.position.x - 20
        slide.duration = 0.3
        slide.autoreverses = true
        slide.repeatCount = Float.infinity
        
        // add slide animatin to square layer
        square.addAnimation(slide, forKey: nil)
        
        // configure scale animation
        let scaleTransform = CATransform3DMakeScale(1.5, 0.5, 0.25)
        
        let zoomScale = CABasicAnimation(keyPath: "transform")
        zoomScale.fromValue = NSValue(CATransform3D: square.transform)
        zoomScale.toValue = NSValue(CATransform3D: scaleTransform)
        zoomScale.duration = 2.0
        zoomScale.autoreverses = true
        zoomScale.repeatCount = Float.infinity
        zoomScale.additive = false
        
        // add zoomscale animation to square layer
        square.addAnimation(zoomScale, forKey: nil)
        
        // configure replicator and add square layer to sublayers
        let replicator = CAReplicatorLayer()
        replicator.addSublayer(square)
        
        replicator.bounds = CGRect(x: 0, y: 0, width: rWidth, height: sideLength)
        replicator.position = CGPoint(x: view.bounds.width / 2, y: sideLength / 2)
        
        replicator.instanceCount = Int(view.bounds.width / sideLength) + 1
        replicator.instanceTransform = CATransform3DMakeTranslation(sideLength - 4, 0, 0)
        replicator.instanceDelay = 0.1
        
        replicator.instanceColor = UIColor.lightBlueishColor().CGColor
        replicator.instanceAlphaOffset = -(1 / Float(replicator.instanceCount) / 2)
        replicator.instanceRedOffset = 1 / Float(replicator.instanceCount) / 2
        
        // configure metaReplicator and add replicator to its sublayers
        let metaReplicator = CAReplicatorLayer()
        metaReplicator.addSublayer(replicator)
        
        metaReplicator.bounds = view.bounds
        metaReplicator.position = view.center
        
        metaReplicator.instanceCount = Int(view.bounds.height / sideLength) + 1
        metaReplicator.instanceTransform = CATransform3DMakeTranslation(0.0, sideLength, 0.0)
        metaReplicator.instanceDelay = 0.3
        
        metaReplicator.instanceAlphaOffset = -(1 / Float(metaReplicator.instanceCount) / 2)
        
        // append metaReplicator to replicatorLayers then add to view's sublayers
        replicatorLayers.append(metaReplicator)
        view.layer.addSublayer(metaReplicator)
    }
}
