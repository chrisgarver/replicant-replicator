//
//  UIColor+CustomColors.swift
//  Replicant Replicator
//
//  Created by Chris Garver on 8/30/15.
//  Copyright © 2015 Bottle Rocket Studios. All rights reserved.
//

import UIKit

extension UIColor {
    
    class func lightBlueishColor() -> UIColor {
        return UIColor(hue: 0.5568, saturation: 0.4727, brightness: 0.9499, alpha: 1.0)
    }
    
    class func darkBlueishColor() -> UIColor {
        return UIColor(hue: 0.542, saturation: 0.903, brightness: 0.8199, alpha: 1.0)
    }
}
